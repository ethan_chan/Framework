<?php
/**
 * 入口文件
 * User: Ethan
 * Date: 2016/9/8 0008
 * Time: 9:40
 * Email： touch_789@163.com
 * 欢迎使用E框架
 */

define("ROOT",__DIR__);                 //根目录
define("APP_PATH",'./App');             //设置项目目录
define('APP_NAME','app');               //设置项目名称
define("CORE",'/Core');                 //设置核心文件目录
define("VENDOR_PATH",CORE."/Vendor/");  //定义第三方类目录
define("DEBUG",true);                   //定义调试模式,true为开启
require './Core/Core.php';              //引入自动加载文件
\Core\Core::run();                      //启动框架
