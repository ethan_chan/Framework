<?php
/**
 * 自动加载类
 * User: Ethan
 * Date: 2016/9/8 0008
 * Time: 9:46
 * Email： touch_789@163.com
 */
namespace Core;
class Autoload {
    private function __construct(){}
    private function __clone(){}
    public static function load($class){

        $name  =  strstr($class, '\\', true);

        if(!in_array($name,array("Vendor")) && !empty($name)){
            $path  = ROOT;
            $file = $path.'/'.str_replace('\\','/', $class).'.php';
            if(true == DEBUG) echo $file."<br/>";
            require  $file;
        }
    }
}