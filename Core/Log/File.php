<?php
/**
 * Created by PhpStorm.
 * User: Ethan
 * Date: 2016/9/13 0013
 * Time: 11:43
 * Email： touch_789@163.com
 */

namespace Core\Log;


class File implements Loginterface
{

    /**
     * 写入日志
     * @param $contentd
     */
    public static function write($content)
    {
        $path = \Core\Configure::get('LOG_PATH');
        if(!file_exists($path)){
            try {
                $error = '/(ToT)/~~ Log not found'.$path;
                throw new \Exception($error);
            } catch (\Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(),'<br>';
            }
        }
        $pathName = $path.date("Ymd",time()).'.txt';
        $contents= "/**----------".PHP_EOL.$content.PHP_EOL."----------**/".PHP_EOL;

        file_put_contents($pathName,$contents.PHP_EOL,FILE_APPEND);
    }



}