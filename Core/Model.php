<?php
/**
 * 数据库模型类.
 * User: Ethan
 * Date: 2016/9/20 0020
 * Time: 10:14
 * Email： touch_789@163.com
 */

namespace Core;
use Core\Configure;
use Core\Vendor\MedooDrive;

class Model
{
    protected  static $databases=null;//实例化对象
    protected  $table = array(); //数据表
    protected  $mapArr = array(
        "where"=>"",
        'field'=>"*",

    );//映射对象

    public function __construct($table="")
    {
        $this->table = $table;
        if(!isset(self::$databases)){
          //  echo '<br/>111<br/>';
            $option = [
                // 必须配置项
                'database_type' =>Configure::get('DB_TYPE'),
                'database_name' => Configure::get('DB_NAME'),
                'server' =>Configure::get('DB_SERVER'),
                'username' =>Configure::get('DB_USER'),
                'password' =>Configure::get('DB_PWD'),
                'charset' =>Configure::get('DB_CHARSET'),
                // 可选参数
                'port' => Configure::get('DB_PORT'),
                // 可选，定义表的前缀
                'prefix' => Configure::get('DB_PREFIX'),
            ];
            /*$objName = "\\Core\\Vendor\\".Configure::get('DB_DRIVER_TYPE').'Drive';
            $obj = new $objName();//实例化驱动
            $this->databases = $obj->run($option);//启用驱动*/
            self::$databases =  MedooDrive::run($option);//启用驱动
        }


    }

    /**
     * 查询条件
     * @param string $where
     * @return $this
     */
    public function where($where=""){
        $this->mapArr['where'] = $where;
        return $this;
    }

    /**
     * 查询多条语句
     * @return array
     */
    public function select(){
       return self::$databases->select($this->table,$this->mapArr['field'],$this->mapArr['where']);
    }

    /**
     * 查询一条数据
     * @return mixed
     */
    public function find(){
        return self::$databases->get($this->table,$this->mapArr['field'],$this->mapArr['where']);
    }

    /**
     * 按字段查询
     * @param string $field
     * @return $this
     */
    public function field($field='*'){
        $this->mapArr['field'] = $field;
        return $this;
    }

    /**
     * 插入数据
     * @param $data array 数据内容
     */
    public function add($data){
        self::notice($data);
        return self::$databases->insert($this->table,$data);
    }


    public function save($data){
        self::notice($data);
        self::notice($this->mapArr['where'],"where");
        return self::$databases->update($this->table,$data,$this->mapArr['where']);
    }

    /**
     * 删除一条数据，谨慎操作
     */
    public function delete(){
        self::notice($this->mapArr['where'],"where");
        return self::$databases->delete($this->table,$this->mapArr['where']);
    }

    /**
     * 原生SQL
     * @param $sql string 原生SQL
     */
    public function query($sql){
        return self::$databases->query($sql);
    }

    /**
     * 异常提示
     * @param $data
     */
    private static function notice($data,$field="data"){
        $notice = "/(ㄒoㄒ)/~~ {$field} is not null";
        if(empty($data)) throw new \Exception($notice);
    }


}