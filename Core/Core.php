<?php
/**
 * 框架启动文件.
 * User: Ethan
 * Date: 2016/9/8 0008
 * Time: 9:45
 * Email： touch_789@163.com
 */
namespace Core;
require './Core/Common/function.php';//引入函数库
require './Core/Autoload.php';//引入自动加载文件
spl_autoload_register("\Core\Autoload::load");

class Core{

    public static $assignArray=array();

    public function __construct()
    {
        //header("X-Powered-By:E FrameWork");
        self::_debug();
    }

    /**
     * 框架启动
     */
    public static function run(){
      
        //载入路由
       $RouteObject =  Factory::getRoute();
       //载入控制器
        $CtrlPath = APP_PATH."/Controller/".$RouteObject->controller.'Controller.php';
        if(file_exists($CtrlPath)){
            $ActionName= $RouteObject->action;
            $appController =ucfirst(strtolower(APP_NAME))."\\Controller\\".$RouteObject->controller.'Controller';
            $obj = new $appController();
            $obj->$ActionName();
            //写入日志
            self::writelog();
        }else{
            try {
                $error = '/(ToT)/~~ Controller not found'.$CtrlPath;
                throw new \Exception($error);
            } catch (\Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(),'<br>';
            }
        }
    }

    /**
     * 写入访问日志
     */
    private static function writelog(){
        $text = "[".getnow()."| ACCESS] : ".$_SERVER['SERVER_ADDR'].":".$_SERVER['SERVER_PORT'];
        Log::writeLog($text);
    }


    /**
     * 载入变量
     * @param 变量名称
     * @param 变量值
     */
    public function assign($name,$value){
        self::$assignArray[$name]=$value;
    }

    /**
     * 载入视图文件
     * @param $文件名称
     */
    public function display($file=null){
        if(empty($file)){
            $routeObj =  Factory::getRoute();
            $file = $routeObj->action.'.html';
        }
        $filePath = APP_PATH.'/View/'.$file;
        if(is_file($filePath)){
           //将数组输出为变量
            extract(self::$assignArray);
            require $filePath;
        }
    }

    /**
     * 全局调试模式
     */
    private static function _debug(){
        DEBUG?ini_set("display_errors",'On'):ini_set("display_errors",'Off');
    }


}