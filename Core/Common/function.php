<?php
/**
 * 函数主库
 * User: Ethan
 * Date: 2016/9/8 0008
 * Time: 9:45
 * Email： touch_789@163.com
 */

/**
 * 浏览器友好的变量输出
 * @param mixed $var 变量
 * @param boolean $echo 是否输出 默认为True 如果为false 则返回输出字符串
 * @param string $label 标签 默认为空
 * @param boolean $strict 是否严谨 默认为true
 * @return void|string
 */
function dump($var, $echo=true, $label=null, $strict=true) {
    $label = ($label === null) ? '' : rtrim($label) . ' ';
    if (!$strict) {
        if (ini_get('html_errors')) {
            $output = print_r($var, true);
            $output = '<pre>' . $label . htmlspecialchars($output, ENT_QUOTES) . '</pre>';
        } else {
            $output = $label . print_r($var, true);
        }
    } else {
        ob_start();
        var_dump($var);
        $output = ob_get_clean();
        if (!extension_loaded('xdebug')) {
            $output = preg_replace('/\]\=\>\n(\s+)/m', '] => ', $output);
            $output = '<pre>' . $label . htmlspecialchars($output, ENT_QUOTES) . '</pre>';
        }
    }
    if ($echo) {
        echo($output);
        return null;
    }else
        return $output;
}

/**
 * 获取当前时间
 * Y-m-d H:i:s
 */
function getnow($timeFormat="Y-m-d H:i:s"){
    return date($timeFormat,time());
}


/**
 * 快速导入第三方框架类库 所有第三方框架的类库文件统一放到 系统的Vendor目录下面
 * @param string $class 类库
 * @param string $baseUrl 基础目录
 * @param string $ext 类库后缀
 * @return boolean
 */
function vendor($class, $baseUrl = '', $ext='.php') {
    if (empty($baseUrl)) $baseUrl = VENDOR_PATH;
    //echo $baseUrl.$class.$ext."<br/>";
    require $baseUrl.$class.$ext;
}

/**
 * 获取Hprose客户端对象
 * @param $url
 * @return Object 返回实例化对象
 */
function getHprose($url){
    vendor('Hprose/HproseHttpClient');
    return new \HproseHttpClient($url);
}

/**
 * 设置缓存
 *
 */
function cache($key,$value="",$expire=null){
    if(empty($key))throw new \Exception("key not null");
    $obj =\Core\Factory::getCache();
    $result =$obj->connect();
    if( is_null($value)){

        return $result->del($key);
    }elseif(empty($value) || $value === ""  ){

        return  $result->get($key);
    }else{
        return $result->set($key,$value,$expire);
    }
}

/**
 * 启动redis实例化工具
 * 启动既链接
 */
function getRedis(){
    $redisObj =  \Core\Factory::getRedis();
    return $redisObj->redisInitobj;
}