<?php
/**
 * 缓存基类.
 * User: Ethan
 * Date: 2016/9/23 0023
 * Time: 16:10
 * Email： touch_789@163.com
 */

namespace Core;
use Core\Core;
use Core\Configure;

class Cache
{
    public $object = null;
    /**
     * 实例化缓存
     */
    public function connect()
    {
        if(!isset($this->object)){
            $objName = "\\Core\\Vendor\\".ucfirst(Configure::get("CACHE_TYPE")).'Drive';
            $this->object = new $objName();
        }
        return $this->object;
    }
}