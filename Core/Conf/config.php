<?php
/**
 * 全局配置文件.
 * User: Ethan
 * Date: 2016/9/12 0012
 * Time: 10:39
 * Email： touch_789@163.com
 */
return array(
    /*---ROUTE-----*/
    'CTRL_NAME'=>'index',
    'ACTION_NAME'=>'index',
    /*-----LOG-----*/
    'LOG_TYPE'=>'file',//目前仅支持file
    'LOG_PATH'=>'./App/Runtime/Log/',
   // 'LOG_SIZE'=>'2048',//文件切割大小，单位 kb
    /*------DATABASES----*/
    'DB_TYPE'=>'mysql',
    'DB_NAME'=>'',
    'DB_SERVER'=>'localhost',
    'DB_USER'=>'',
    'DB_PWD'=>'',
    'DB_CHARSET'=>'utf8',
    'DB_PORT'=>'3306',
    'DB_PREFIX'=>'',
    /*-----CACHE ---------*/
    'CACHE_TYPE'=>'redis',
    'CACHE_ADDRESS'=>"127.0.0.1",
    'CACHE_PWD'=>'',//缓存授权密码,可为空
    "CACHE_PORT"=>'6379',

);