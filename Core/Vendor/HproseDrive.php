<?php

/**
 * Hprose 驱动类
 * User: Ethan
 * Date: 2016/9/19 0019
 * Time: 11:10
 * Email： touch_789@163.com
 */
namespace Core\Vendor;

class HproseDrive
{
    protected $allowMethodList  =   '';
    protected $crossDomain      =   false;
    protected $P3P              =   false;
    protected $get              =   true;
    protected $debug            =   false;

    /**
     * 架构函数
     * @access public
     */
    public function __construct() {
        //导入类库
        vendor('Hprose/hproseHttpServer');
        //实例化HproseHttpServer
        $server     =   new \HproseHttpServer();
        if($this->allowMethodList){
            $methods    =   $this->allowMethodList;
        }else{
            $methods    =   get_class_methods($this);
            $methods    =   array_diff($methods,array('__construct','__call'));
        }
        $server->addMethods($methods,$this);
        if(true == DEBUG) {
            $server->setDebugEnabled(true);
        }
        // Hprose设置
        $server->setCrossDomainEnabled($this->crossDomain);
        $server->setP3PEnabled($this->P3P);
        $server->setGetEnabled($this->get);
        // 启动server
        $server->start();
    }
}