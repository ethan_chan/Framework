<?php

/**
 * Created by PhpStorm.
 * User: ethanchan
 * Date: 2016/10/16
 * Time: 23:22
 */

namespace Core\Vendor\Init;
use Core\Configure;

class RedisInit
{
    
    public $redisInitobj =null;
    public function __construct()
    {
            $redis = New \Redis();
            $result = $redis->connect(Configure::get("CACHE_ADDRESS"),Configure::get("CACHE_PORT"));
            if(!$result)throw new \Exception("Redis connect fail!");
            //如果设置密码则授权
            if(!empty($pwd = Configure::get("CACHE_PWD"))) {
                $pwd = $redis->auth($pwd);
                if (!$pwd) throw new \Exception("Redis auth error!");
            }
            $this->redisInitobj = $redis;
        }
}