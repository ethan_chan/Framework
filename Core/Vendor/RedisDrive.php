<?php
/**
 * Redis 驱动类
 * 本类缓存仅支持string缓存
 * User: ethanchan
 * Date: 2016/10/13
 * Time: 10:13
 */

namespace Core\Vendor;
use Core\Configure;


class RedisDrive
{
    public $obj = null;

    //初始化redis预设
    public function __construct()
    {
        $redisobj =new \Core\Vendor\Init\RedisInit();
        $this->obj = $redisobj->redisInitobj;
    }
    
    /**
     * redis设置缓存
     * @key key值
     * @vals 缓存值
     * @expire 过期时间
     */
    public function set($key,$vals,$expire=null){
        return $this->obj->set($key,$vals,$expire);
    }


    /**
     * redis获取缓存
     */
    public function get($key){
        return $this->obj->get($key);
    }

    /**
     * 删除指定key的缓存
     * @param $key
     * @return int
     */
    public function del($key){
        return $this->obj->del($key);
    }

}