<?php
/**
 * Medoo数据库驱动
 * User: Ethan
 * Date: 2016/9/20 0020
 * Time: 9:43
 * Email： touch_789@163.com
 */
namespace Core\Vendor;

class MedooDrive
{

    public static function run($option){
        vendor("Medoo/medoo");
        return  new \medoo($option);
    }


}