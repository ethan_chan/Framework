<?php

/**
 * Memcache 驱动类.
 * User: Ethan
 * Date: 2016/9/23 0023
 * Time: 15:42
 * Email： touch_789@163.com
 */
namespace Core\Vendor;
use Core\Configure;
class MemcacheDrive
{
    public function __construct()
    {
        $obj = new \Memcache();
        $result =$obj->connect(Configure::get("CACHE_ADDRESS"),Configure::get("CACHE_PORT"));
        if(!$result){
            throw new \Exception("Memcached connect fail");
        }else{
            $this->object=$obj;
        }
    }

    /**
     * 设置缓存
     * 参数：
     *$key ：将要存储的键值。
     *$var ：存储的值，字符型和整型会按原值保存，其他类型自动序列化以后保存。
     *$flag：是否用MEMCACHE_COMPRESSED来压缩存储的值，true表示压缩，false表示不压缩。
     *$expire：存储值的过期时间，如果为0表示不会过期，你可以用unix时间戳或者描述来表示从现在开始的时间，但是你在使用秒数表示的时候，不要超过2592000秒 (表示30天)。
     */
    public function set($key,$value,$expire=null,$is_flag=false){

        return $this->object->set($key,$value,$is_flag,$expire);
    }

    /**
     * @param $key
     */
    public function get($key){
        return $this->object->get($key);
    }

    public function del($key){
        return $this->object->delete($key);
    }

}