<?php
/**
 * 配置文件基类\
 * 读取 \Core\Conf\config.php
 * 读取控制器下的config.php 优先级1
 * 合并数组
 */
namespace Core;
class Configure{

    public static $ConfigArr =array();
    public static $countConfig = 0;
    /**
     * 获取配置信息
     * @param $name 配置名称
     * @param $value 配置值
     */
    public static function get($name){
        if(!self::$ConfigArr){
            $result =  self::_getConfigArray($name);
            if($result){
               return self::$ConfigArr[$name];
            }else{
                try {
                    $error = '/(ToT)/~~ Config file not found'.$name;
                    throw new \Exception($error);
                } catch (\Exception $e) {
                    echo 'Caught exception: ',$e->getMessage(),'<br>';
                }
            }
        }else{
            return self::$ConfigArr[$name];
        }
    }


    /**
     * 设置配置文件的内容，文件修改级别
     * 默认修改到APP配置文件
     * @param $name 配置名称
     * @param $value  配置值
     * @param $is_orveall 是否修改到全局配置
     */
    public static function set($name,$value){
        self::$ConfigArr[$name]=$value;
        return  self::$ConfigArr[$name];
    }

    /**
     * 拉取/更新配置文件
     * @param $name
     */
    private static function _getConfigArray($name){
        $path = '.'.CORE."/Conf/config.php";
        $AppConfig =APP_PATH."/Conf/config.php";
        if(is_file($path)){
            $conArr = require $path; //读取全局配置文件
            if(is_file($AppConfig)){ //读取APP配置文件
                $appConfig = require $AppConfig;
                $conArr =array_merge($conArr,$appConfig);
            }
            self::$ConfigArr=$conArr;
            return  self::$ConfigArr[$name];
        }else{
            return false;
        }
    }

}