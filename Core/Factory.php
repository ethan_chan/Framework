<?php
/**
 * 工厂模式
 * User: Ethan
 * Date: 2016/9/8 0008
 * Time: 10:00
 * Email： touch_789@163.com
 */
namespace Core;

class Factory{

    private static $objectArr = array();

    //获取路由对象
    public static function getRoute(){
     return self::newOjbect("route","Route");
    }

    /**
     * 加载配置文件
     */
    public static function getConfig(){
         return self::newOjbect("config","Configure");
    }

    /**
     * 获取缓存驱动
     */
    public static function getCache(){
        return self::newOjbect("cache","Cache");
    }

    /**
     * 实例化redis驱动
     * @return object
     */
    public static function getRedis(){
        return self::newOjbect("redis","RedisInit","\\Core\\Vendor\\Init\\");
    }

    /**
     * 实例化对象方法
     * @param $key 对象key
     * @param $objName 对象名称
     * @param string $namespace 对象命名空间
     * @return mixed
     */
    protected static function newOjbect($key,$objName,$namespace="\\Core\\"){
        $objName = $namespace.$objName;
        if(!isset(self::$objectArr["$key"])){
            self::$objectArr["$key"]= new $objName();
        }
        $obj= self::$objectArr["$key"];
        return $obj;
    }


}