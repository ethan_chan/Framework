<?php
/**
 * 日志驱动
 * User: Ethan
 * Date: 2016/9/13 0013
 * Time: 11:35
 * Email： touch_789@163.com
 */

namespace Core;

class Log
{
  //  public $thread = ""; //线程记录


    /**
     * 写入日志
     * @param $content 日志内容
     */
    public static function writeLog($content){
        $type = \Core\Configure::get('LOG_TYPE');
        if($type == "file"){
            \Core\Log\File::write($content);
        }
    }



}