<?php
/**
 * 路由类.
 * User: Ethan
 * Date: 2016/9/8 0008
 * Time: 9:55
 * Email： touch_789@163.com
 */
namespace Core;
class Route{

    public $controller = null;//控制器
    public $action = null;//方法

    public function __construct()
    {


        $path =$_SERVER['REQUEST_URI'];
        if( isset($path) && $path !='/' ){
            $pathArr = explode("/",trim($path,'/'));
            if(!empty($pathArr[0])){
                $this->controller=ucfirst(strtolower($pathArr[0]));
                unset($pathArr[0]);
            }
            if(empty($pathArr[1])){
                $this->action=\Core\Configure::get('ACTION_NAME');
            }else{
                $this->action=$pathArr[1];
                unset($pathArr[1]);
            }
            //过滤GET参数
          $pathArr=array_values($pathArr);
            for($i=0 ; $i<count($pathArr);$i+=2){
                if(!empty($pathArr[$i+1])){
                    $_GET[$pathArr[$i]] = $pathArr[$i+1];
                }
            }
        }else{
            $this->controller =\Core\Configure::get('CTRL_NAME');
            $this->action=\Core\Configure::get('ACTION_NAME');
        }
    }


    private static function actionName($name,$is_action = true){
       return ucfirst(strtolower($name));
    }

}