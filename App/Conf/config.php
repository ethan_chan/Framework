<?php
/**
 * 配置文件
 * User: Ethan
 * Date: 2016/9/12 0012
 * Time: 11:11
 * Email： touch_789@163.com
 */
return array(
    'TEXT'=>'test',
    'TEXT2'=>'tes31231212t',

    'DB_TYPE'=>'mysql',
    'DB_NAME'=>'game',
    'DB_SERVER'=>'localhost',
    'DB_USER'=>'root',
    'DB_PWD'=>'root',
    'DB_CHARSET'=>'utf8',
    'DB_PORT'=>'3306',
    'DB_PREFIX'=>'xrh_',

    'CACHE_TYPE'=>'redis',
    'CACHE_ADDRESS'=>"",
    'CACHE_PWD'=>'',//缓存授权密码,可为空
    "CACHE_PORT"=>'6379',

);