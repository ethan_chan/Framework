﻿## 简介
###没有找到LOGO，将就这个吧。
![image](http://wx.086jr.com/Public/images/git_logo.jpg)


###E Frame 是一个免费开源的，快速、简单的面向对象的 轻量级PHP开发框架 ，创立于2016年初，遵循Apache2开源协议发布，是为了敏捷API应用开发和简化企业应用开发而诞生的。
###代码风格类似Thinkphp，让用户更快的上手。

##目录

    --App [项目目录]
        -- Conf  [配置文件]
        -- Controller [控制器文件]
        -- Runtime [缓存目录]
        -- View     [视图目录]
    -- Core [核心目录]
        -- Common [核心方法]
        -- Conf [核心配置文件]
        -- Log [日志处理]
        -- Vendor [第三方类]

##配置文件
设置配置文件在 App/Conf 目录下
###获取配置

    \Core\Configure::get("配置键名");


##数据库操作
###数据库配置

    'DB_TYPE'=>'mysql',  //数据库类型（目前仅支持mysql）
    'DB_NAME'=>'',       //数据库名称
    'DB_SERVER'=>'',    // 数据库地址
    'DB_USER'=>'',      // 数据库用户名
    'DB_PWD'=>'',       //数据库密码
    'DB_PREFIX'=>'',    //数据库前缀（如无前缀请填写null）

### 数据模型

    new \Core\Model('DbName'); //实例化数据模型

目前支持 select , where , field , find , save ,add, delete , query 方式的链式操作。


##Hprose
Hprose是一款先进的轻量级、跨语言、跨平台、无侵入式、高性能动态远程对象调用引擎库。它不仅简单易用，而且功能强大。
使用方法：
一、生成Hprose服务端

直接继承HproseDrive方法，该控制器直接变为Hprose服务器。
ps: 请注意是return并非echo

    class IndexController extends \Core\Vendor\HproseDrive {
        public function test1(){
            return 'hello world';
        }
    }

二、获取Hprose内容
无需继承Hprose父类，直接实例化访问；

    public function index(){
        $client =getHprose('http://127.0.0.1/Index/index');
        $result = $client->test1(); //调用服务端的方法
        dump($result);
    }

##缓存
###缓存目前支持Memcache和Redis，使用方法简单
*   key 缓存的键名
*   value 缓存的值
*   expire 缓存时间（秒）
###cache($key,$value,$expire);

一、获取缓存
cache("keyName");
二、设置缓存
cache("keyName","hello world",60);
三、删除缓存
cache("keyName",null);

###Redis实例化
只需在配置文件中写入redis的账号密码和地址,然后使用 getRedis() 函数即可

    $redis = getRedis();
    dump($redis->info());


